<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>thema ecommerce bootstrap</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Roboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!--bootstrap-->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/js/bootstrap.bundle.min.js"></script>

    <!--jquery-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('assets/css/all.css') }}">

</head>

<body>

    <header>
        <div id="header">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light justify-content-between">
                    <div class="container">
                        <a class="navbar-brand" href="#">
                            <!--Meu Delivery-->
                            <div class="logo">
                                <h1>Laravel
                                    <span class="logo2">Ecommerce</span>
                                </h1>
                            </div>
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-principal" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fas fa-bars"></i>
                        </button>

                        <div class="collapse navbar-collapse" id="nav-principal">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" style="cursor:pointer;"> HOME </a>
                                </li>

                                <li class="nav-item">
                                    <a style="cursor:pointer;" class="nav-link">ABOUT</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">BLOG</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="fas fa-user"></i>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <div class="bag-store">
                                        <a href="#" class="nav-link">
                                            <i class="fas fa-shopping-bag fa-lg"></i>
                                            <span>3</span>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div align="right">
                    <form class="d-flex product-serch">
                        <input class="form-control" type="search" placeholder="Procurar produto" aria-label="Search">
                        <button class="btn btn-outline-warning ml-2" type="submit"><i class="fas fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
        <!--endnav-->

        <div class="container">
            <div class="slide">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Thema bootstrap ecommerce</h1>
                        <p>praticando a criação de um ecommerce em Laravel</p>
                        <div class="buttons-slide">
                            <a href="#" class="button-primary button-white">Button 1</a>
                            <a href="#" class="button-primary button-white">Button 2</a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="slide-image">
                            <img src="assets/img/macbook-pro.png" alt="hero image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header><!-- end header-->

    <div class="featured-section">
        <div class="container mt-5 mb-5">
            <div class="top-featured-products text-center">
                <div class="top-featured-products-title mb-3">
                    <h1>Produtos em destaque</h1>
                </div>
                <div class="top-featured-products-description mb-3">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore cum placeat, nisi debitis iste deleniti provident inventore earum veritatis iusto illo facilis delectus itaque ipsa suscipit mollitia consequatur incidunt doloremque.
                </div>
                <div class="buttons-featured-product">
                    <a href="#" class="button-primary button-gray">Destaque</a>
                    <a href="#" class="button-primary button-gray">Promoção</a>
                </div>
            </div>
            <div class="featured-products">
                <div class="products">
                    <div class="row">
                        <div class="col-xl-3 col-md-6 col-sm-12">
                            <div class="product">
                                <a href="#"><img src="assets/img/macbook-pro-sale.jpg" alt="product"></a>
                                <a href="#">
                                    <div class="product-name text-center">MacBook Pro</div>
                                </a>
                                <div class="product-price text-center">R$ 2499,99</div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 col-sm-12">
                            <div class="product">
                                <a href="#"><img src="assets/img/macbook-pro-sale.jpg" alt="product"></a>
                                <a href="#">
                                    <div class="product-name text-center">MacBook Pro</div>
                                </a>
                                <div class="product-price text-center">R$ 2499,99</div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 col-sm-12">
                            <div class="product">
                                <a href="#"><img src="assets/img/macbook-pro-sale.jpg" alt="product"></a>
                                <a href="#">
                                    <div class="product-name text-center">MacBook Pro</div>
                                </a>
                                <div class="product-price text-center">R$ 2499,99</div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 col-sm-12">
                            <div class="product">
                                <a href="#"><img src="assets/img/macbook-pro-sale.jpg" alt="product"></a>
                                <a href="#">
                                    <div class="product-name text-center">MacBook Pro</div>
                                </a>
                                <div class="product-price text-center">R$ 2499,99</div>
                            </div>
                        </div>

                        <div class="col-xl-3 col-md-6 col-sm-12">
                            <div class="product">
                                <a href="#"><img src="assets/img/macbook-pro-sale.jpg" alt="product"></a>
                                <a href="#">
                                    <div class="product-name text-center">MacBook Pro</div>
                                </a>
                                <div class="product-price text-center">R$ 2499,99</div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 col-sm-12">
                            <div class="product">
                                <a href="#"><img src="assets/img/macbook-pro-sale.jpg" alt="product"></a>
                                <a href="#">
                                    <div class="product-name text-center">MacBook Pro</div>
                                </a>
                                <div class="product-price text-center">R$ 2499,99</div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 col-sm-12">
                            <div class="product">
                                <a href="#"><img src="assets/img/macbook-pro-sale.jpg" alt="product"></a>
                                <a href="#">
                                    <div class="product-name text-center">MacBook Pro</div>
                                </a>
                                <div class="product-price text-center">R$ 2499,99</div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 col-sm-12">
                            <div class="product">
                                <a href="#"><img src="assets/img/macbook-pro-sale.jpg" alt="product"></a>
                                <a href="#">
                                    <div class="product-name text-center">MacBook Pro</div>
                                </a>
                                <div class="product-price text-center">R$ 2499,99</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end-products-->
    </div>


    <div class="blog-section">
        <div class="container mt-5 mb-5">
            <div class="top-blog-section text-center">
                <div class="top-blog-section-title mb-3">
                    <h1>Nosso Blog</h1>
                </div>

                <div class="top-blog-section-description mb-3">
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae quas repudiandae perspiciatis dolor illum fuga autem maiores quibusdam, esse adipisci reiciendis vitae totam quae enim fugit voluptatibus excepturi a. Reiciendis.
                    </p>
                </div>

                <div class="blog-posts">
                    <div class="row">
                        <div class="col-xl-4 col-md-4 col-sm-12">
                            <div class="blog-post">
                                <a href="#"><img src="assets/img/blog1.jpg"></a>
                                <a href="#">
                                    <h2 class="blog-title text-center">Blog Post Title 1</h2>
                                </a>
                                <div class="blog-description text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est ullam, ipsa quasi?</div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-4 col-sm-12">
                            <div class="blog-post">
                                <a href="#"><img src="assets/img/blog2.jpg"></a>
                                <a href="#">
                                    <h2 class="blog-title text-center">Blog Post Title 2</h2>
                                </a>
                                <div class="blog-description text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est ullam, ipsa quasi?</div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-4 col-sm-12">
                            <div class="blog-post">
                                <a href="#"><img src="assets/img/blog3.jpg"></a>
                                <a href="#">
                                    <h2 class="blog-title text-center">Blog Post Title 3</h2>
                                </a>
                                <div class="blog-description text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est ullam, ipsa quasi?</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--endblog-->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="made-with" style="cursor: pointer;">by <i class="fas fa-paint-brush"></i> Wesley Sanches</div>
                </div>
                <div class="col-md-6">
                    <div class="social">
                        <span>Follow me : </span>
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fab fa-gitlab"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>

</html>

<script>
    jQuery(document).ready(function($) {
        window.onscroll = function() {
            if (window.pageYOffset > 140) {
                $("#header").addClass("active");
            } else {
                $("#header").removeClass("active");
            }
        }
    });
</script>